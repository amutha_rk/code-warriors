package Static;

class Employee {
	int id;
	String name;
	static String company_name="Talent sculptor";
	Employee(int id,String name){
		this.id=id;
		this.name=name;
	}
	void show_details() {
		System.out.println(id);
		System.out.println(name);
		System.out.println(company_name);
	}
	

	public static void main(String[] args) {
		Employee e1=new Employee(1,"amutha");
		Employee e2=new Employee(2,"abi");
		Employee e3=new Employee(3,"nandini");
		e1.show_details();
		
		// TODO Auto-generated method stub

	}

}
