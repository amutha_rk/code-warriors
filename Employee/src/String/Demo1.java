package String;

public class Demo1 {

	public static void main(String[] args) {
		String s1="John";
		String s2=new String("Hello");
		String s3=new String("Hello");
		System.out.println(s2==s3);
		System.out.println(s2.equals(s3));
		s1.concat("Rock");
		System.out.println(s1);
		s1=s1.concat("abc");
		StringBuffer sb=new StringBuffer("abc");
		System.out.println(sb.length());
		StringBuilder builder=new StringBuilder("defgh");
		builder.append("java");
		System.out.println(builder);
		
		
		char b=s1.charAt(2);
		System.out.println(b);
		
		// TODO Auto-generated method stub

	}

}
